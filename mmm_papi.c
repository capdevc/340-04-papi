/* mmm_papi.c -- CS340 PAPI Assignment */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <papi.h>

/* This code initializes a square matrix of size n*n */
/* so each element Aij equals i*j+i                  */
static void init_square_matrix(double* A, int n) {
   
   int i,j;
   
   for(j = 0; j < n; j++) {
       for (i = 0; i < n; i++) {
	 A[(j*n) + i] = (double)(i*j+i);
      }
   }
}

/* This code initializes a square matrix of size n*n */
/* so each element Aij equals i*j+i                  */
static void init_transpose_matrix(double* A, int n) {
   
   int i,j;
   
   for(j = 0; j < n; j++) {
       for (i = 0; i < n; i++) {
	 A[(i*n) + j] = (double)(i*j+i);
      }
   }
}

/* This code does a naive square matrix-matrix multiply of */
/* matrices of size n x n                                  */
void naive_dgemm(int n, double *A, double *B, double *C) {
   
   int i,j,k;
   double s;

   for(i=0;i<n;i++) {
      for(j=0;j<n;j++) {

         s=0.0;
         for(k=0;k<n;k++) {
	    /* Cij = Cij + Aik + Bkj */
            s+=A[(k*n)+i]*B[(j*n)+k];
         }
         C[(j*n)+i]=s;
      }
   }
}

/* This code does a naive square matrix-matrix multiply of */
/* matrices of size n x n                                  */
void transpose_dgemm(int n, double *A, double *B, double *C) {
   
   int i,j,k;
   double s;

   for(i=0;i<n;i++) {      
      for(j=0;j<n;j++) {

         s=0.0;
         for(k=0;k<n;k++) {
	    /* Cij = Cij + Aik + Bkj */
            s+=A[(i*n)+k]*B[(j*n)+k];
         }
         C[(j*n)+i]=s;
      }
   }
}

#define MATRIXSIZE 400

int main(int argc, char** argv) {

   int retval,native;
   long long values[2];
   long long start_virt,stop_virt;
   double flops,seconds;   
   
   double *A,*B,*C,*AT;
   int event_set=PAPI_NULL;

   /* Allocate memory for the matrices */
   A=calloc(MATRIXSIZE*MATRIXSIZE,sizeof(double));
   AT=calloc(MATRIXSIZE*MATRIXSIZE,sizeof(double));
   B=calloc(MATRIXSIZE*MATRIXSIZE,sizeof(double));
   C=calloc(MATRIXSIZE*MATRIXSIZE,sizeof(double));
   
   if ((A==NULL) || (AT==NULL) || (B==NULL) || (C==NULL)) {
      fprintf(stderr,"Error allocating memory!\n");
      exit(1);
   }

   /* Initialize Matrices A and B */
   init_square_matrix(A, MATRIXSIZE);
   init_square_matrix(B, MATRIXSIZE);

   /* Initialize A transpose */
   init_transpose_matrix(AT, MATRIXSIZE);


   /* Initialize PAPI */
   retval = PAPI_library_init(PAPI_VER_CURRENT);
   if (retval != PAPI_VER_CURRENT) {
      fprintf(stderr,"Error with PAPI_library_init\n");
   }

   /* Create an EventSet */
   retval=PAPI_create_eventset(&event_set);
   if (retval != PAPI_OK) {
      fprintf(stderr,"Error with PAPI_create_eventset\n");  
   }

   /* Get the eventcode for PAPI_FP_OPS */
   retval = PAPI_event_name_to_code( "PAPI_FP_OPS", &native );
   if (retval != PAPI_OK) {
     fprintf(stderr,"Error converting PAPI_FP_OPS\n");
   }
  
   /* Add PAPI_FP_OPS to the event_set */
   retval=PAPI_add_event(event_set,native);
   if (retval != PAPI_OK) {
     fprintf(stderr,"Error in PAPI_add_event\n");
   }

   /* Get starting virtual time */
   start_virt=PAPI_get_virt_usec();

   /* Start measuring */
   PAPI_start(event_set);

   naive_dgemm(MATRIXSIZE, A, B, C);
   // transpose_dgemm(MATRIXSIZE, AT, B, C);

   /* Stop measuring, store counts in values array */
   PAPI_stop(event_set,values);

   /* Get stopping virtual time */
   stop_virt=PAPI_get_virt_usec();

   /* calculate some values */
   flops=(double)values[0];
   seconds=((double)(stop_virt-start_virt))/1.0e6;
   
   /* Print Results */
   //printf("%lf %lf\n",C[0],C[MATRIXSIZE*2+5]);
   printf("Elapsed usecs = %lld\n",stop_virt-start_virt);
   printf("Floating Point Operations = %lld\n",values[0]);
   printf("GFLOP/s = %.2f\n",(flops/1.0e9)/seconds);

   return 0;
}
