# Makefile for CS340 PAPI Homework Assignment

CC = gcc
CFLAGS = -Wall -O2 -g
LFLAGS = 
PAPI_CFLAGS = -I/usr/local/include
PAPI_LFLAGS = -L/usr/local/lib -lpapi

all:	mmm_papi


mmm_papi:	mmm_papi.o
	$(CC) $(LFLAGS) $(PAPI_LFLAGS) -o mmm_papi mmm_papi.o

mmm_papi.o:	mmm_papi.c
	$(CC) $(CFLAGS) $(PAPI_CFLAGS) -c mmm_papi.c

clean:	
	rm -f *~ *.o mmm_papi


